﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
document.querySelectorAll('.onclick').forEach(item => {
    item.addEventListener('click', event => {
        setAddress(item)
    })
})
var elementToOpen = document.getElementById(window.location.hash.toLowerCase().substr(1))
if (elementToOpen != null) {
    elementToOpen.parentElement.setAttribute("open","")
}

function setAddress(element) {
    var id= element.id
    var hash="#" + id
    
    if (!element.parentElement.hasAttribute("open")) {
        // So the screen doesn't jump when the hash is added to the address, nothing can be done about the screen resetting on removal
        element.id = ""
        window.location.hash = hash
        element.id = id
    }
}

document.fonts.load("16px inter", "test")
	.then(function() {
		document.documentElement.className += ' inter-loaded';
	});
