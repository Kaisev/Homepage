---
Title: 2. How to Read Web Addresses (URLs)
Synopsis: Learn how to read web addresses and how to use this knowledge to protect yourself from attacks
Author: Jonathan Daigle
Modified: 2023-04-24
---

# Web Addresses

[TOC]

A web address or URL is the address of something on the Internet. This is the address at the top of your webpage, and it tells you some important information about where on the Internet you are, and how you're connected. For example, let's look at the address for Google: `https://www.google.ca/`

From the address we can tell what site we are connected to, and if we are connected securely. This will help you to avoid fake versions of websites that steal people's information (known as phishing sites).

*Tip: Links on webpages lead to addresses. If you move your mouse over the link you will see an indicator in the bottom corner of your screen telling you what address the link leads to.*

## Where are we?

Looking at any web address, there are a series of words and letters separated by periods

``` default
www.google.ca
```

What we care about here is the `google.ca` part. `mail.google.ca`, `maps.google.ca`, and `www.google.ca`, are all sites on `google.ca`. We know this because the last parts of the address before the forward slash (`/`) are `google` and `ca`.

In an example, let's look at `kde.org` and `kde.net`. `kde.org` is owned by KDE e.V., and `kde.net` isn't owned by anyone, and could be purchased by anyone. This means someone could purchase `kde.net` and build a page that looks like a login page for `kde.org` to try and steal usernames and passwords.

It can be hard to remember whether the site you want is `kde.org` or `kde.net`, so it is a good idea to create bookmarks for sites you use and trust, and use them to navigate to those sites. Beware that some fake websites will use ambiguous characters (letters or numbers that look like other letters or numbers), such as a lower case L instead of an I, to trick you.

*Note: Using [a password manager](software) is the best way to protect yourself from this. Your password manager will not prompt you to fill in your password on the wrong site, even if it looks similar to the correct site.*

## HTTP VS HTTPS

You will notice that some addresses have HTTP or HTTPS in front of them, and some addresses have nothing in front of them. This tells you *how* you are connecting to a page. The difference is simple, HTTPS is just HTTP Secure, a secure version of HTTP.

Seeing that `HTTPS` or the green lock in the address bar means two things:

1. This is the website that it says it is in the address bar, and your browser has verified this
2. Your connection is secure, no one can read what you send to the site or what it sends to you

**HTTPS does not mean the site you are on is safe**. It just means that you are on the site the address bar says you're on, and that your connection to that site is secure. This is why you it's important to know how to read the address bar, so you can confirm **both** that you are on the site you trust and that your connection is secure.

If you don't see HTTPS or the little green lock, then neither of the above is true. When you're at `http://google.ca`, you have no way of knowing that you are actually seeing Google, or if it's an imposter. In addition to that, anyone can see what data you send to that page. Always make sure you see `HTTPS` or that green padlock and verify you're on the right page before you enter login or payment information.

*Note: When using a work or school provided device, or any device that someone else has control over, nothing you do is necessarily private. Organizations often configure their devices so that they can access any data that the device handles. HTTPS, private browsing, or other privacy controls will not change that. This is one of many reasons not to use work devices for personal reasons.*
