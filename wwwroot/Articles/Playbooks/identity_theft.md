---
Title: Identity Theft Playbook
Synopsis: A basic playbook of what to do if you or a loved one is a victim of identity theft
Author: Jonathan Daigle
Modified: 2024-09-22
---

# Identity Theft Playbook

[TOC]

This information is largely a summary based on the Government of Canada pages below:  
<https://www.canada.ca/en/revenue-agency/services/forms-publications/publications/rc284/protect-yourself-against-identity-theft.html>  
<https://www.priv.gc.ca/en/privacy-topics/identities/identity-theft/guide_idt/>

The US Government also has fraud resources here:  
<https://www.identitytheft.gov/>

A more detailed list of steps are listed here:  
<https://www.identitytheft.gov/Steps>

There are a lot of people and organizations that you need to contact, by phone is best, as these are urgent matters.

## Contact Financial Institutions

With your banks and trading platforms. Ensure you have flags put on all accounts and freeze any accounts that have been affected.

## Contact Law Enforcement

Open a case with your local law enforcement, and look for any anti-fraud and identity theft reporting channels for your country.

For Canadians, contact the anti-fraud centre (in addition to local law enforcement): <https://antifraudcentre-centreantifraude.ca/report-signalez-eng.htm>

## Contact Credit Reporting Agencies

In Canada the main credit reporting institutions are [Equifax](https://www.consumer.equifax.ca/personal/contact-us) and [TransUnion](https://www.transunion.ca/assistance/fraud-victims-resources).

You should regularly check your credit report with these agencies for any unexpected changes.

## Contact Tax Agencies

In Canada this is the [CRA](https://www.canada.ca/en/revenue-agency/corporate/scams-fraud/report-scam.html#h1), in the US this is the [IRS](https://www.irs.gov/newsroom/taxpayer-guide-to-identity-theft).

## Record Everything

Receipts for any transactions (big and small), payments, and any other financial transactions, contributions, or changes. These will likely be needed by your taxation agency

## Replace your IDs

If your ID numbers have been compromised (such as Personal Health Number, driver's license number, SIN, etc.) you should contact the issuing agency and, if possible, have it re-issued with a new number. Yes, this can include your SIN/SSN as well. In Canada you can call [1-800-622-6232](tel:1-800-622-6232)
