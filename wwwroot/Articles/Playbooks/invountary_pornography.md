---
Title: Involuntary Pornography/Unwanted Content Playbook
Synopsis: A basic playbook of what to do if there is photos or other media being shared or spread that you don't want shared or spread
Author: Jonathan Daigle
Modified: 2024-09-21
---

# Involuntary Pornography/Unwanted Content Playbook

[TOC]

This playbook covers situations in which media of you are being spread or posted online without your consent, in particular content of a sexual or embarrassing nature (i.e.: nudes).

Before starting this playbook, you should start with [Securing your accounts](secure)

## Get a Lawyer(s)

There are two legal avenues to explore here, and so you may end up with two lawyers to handle each specialty:

- Cyber Harassment
- Copyright

Copyright? Yes, that thing everyone hates, it turns out can be useful here. If the media was created by you (i.e.: selfies) or by someone who isn't a bad actor (e.g.: a photo shoot) you will own or may be able to get the rights to the copyright. I will go over that more in [DMCA Takedowns](#dmca-takedowns). Talk to your lawyer about your legal options, opening a lawsuit may enable to you subpoena records to help identify the bad actor.

## Open a Case With Law Enforcement

Ideally you want to do this as soon as possible, but beware this may be a battle. Police forces in North America (and likely globally) are notoriously inadequate at handling cyber issues. Don't be afraid to report your case to federal agencies as well, and make sure to follow up and be persistent, make it hard to be ignored.

## Gather Data

Use reverse image search to find offending posts, check 4chanarchive as well. Search usernames and any other identifying info about your attacker. **Record everything.**

## DMCA Takedowns

If the media was taken by someone else (who isn't a bad actor, such as a photo shoot or a friend) contact that person to see if you can get in writing the release of the copyright to you.

Any sites where the offending content is found, issue a DMCA. Make sure to also issue takedowns for involuntary pornography as well.

Involuntary pornography is illegal in many jurisdictions, but many sites and companies will be more familiar with DMCA, so those requests will probably be processed faster, and still apply in jurisdictions where involuntary pornography isn't illegal.

## Find the Perpetrator

This may be something that you or your trusted people do, or something that a private investigator or your lawyer can help with. We are effectively talking about counter cyber-stalking

TODO: Complete this section, maybe remove it? I don't know.

## Show no Mercy

If someone is doing this to you, that same person likely has or will have other victims as well. Once you reach a point where you can impose consequences, through pressing criminal charges or through a lawsuit, you do not want to stop because their family tells you "they will take care of it" or they promise "it will never happen again".

This isn't about revenge, it's about ensuring that the consequences for their actions stick, and protecting others from their abuse.

## Resources

- [WithoutMyConsent.org](https://withoutmyconsent.org/) - Provide detailed guides for involuntary pornography victims
- [Cyber Civil Rights](https://cybercivilrights.org/ccri-safety-center/) - A US based non-profit that provides guidelines for handling involuntary pornography, as well as a variety of resources
