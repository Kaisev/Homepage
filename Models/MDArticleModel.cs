namespace homepage.Models;

internal partial class MDArticleModel
{
    public string? Name {
        get {return System.IO.Path.GetFileNameWithoutExtension(Path);}
    }
    public bool Read = false;
    public string? Path;
    public string? Title;
    public string? Author;
    public DateTime? Modified;
    public string? Synopsis;

    internal static List<MDArticleModel> GetArticles(string path) {
        List<MDArticleModel> retVal = new();
        foreach (string article in Extensions.GetFilesRecursive(path).OrderBy(x => x)) {
            string content = File.ReadAllText(article);
            int firstIndex = content.IndexOf("---");
            int secondIndex = content.IndexOf("---", firstIndex + 3);
            string yaml = content.Substring(firstIndex, secondIndex);

            var props = new YamlDotNet.Serialization.Deserializer().Deserialize<MDArticleModel>(yaml);
            
            props.Path = MDRegEx().Replace(System.IO.Path.GetRelativePath(path, article), "").Replace("\\", "/");
            
            retVal.Add(props);
        }
        return retVal;
    }

    [System.Text.RegularExpressions.GeneratedRegex("\\.md$")]
    private static partial System.Text.RegularExpressions.Regex MDRegEx();
}
