using Westwind.AspNetCore.Markdown;
using Markdig;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddMarkdown(config => {
    config.ConfigureMarkdigPipeline = builder =>
    {
        builder
        .UseAdvancedExtensions()
        .UseYamlFrontMatter()
        .UseTableOfContent(
            tocAction: opt=>{/*opt.ContainerTag = "nav";*/opt.OverrideTitle = "Contents"; opt.TitleTag = "h2"; /*opt.TocTag = "details"*/;}
        )
        .Build();
    };
    var folderConfig = config.AddMarkdownProcessingFolder("/Articles/", "~/Views/Shared/__MarkdownPageTemplate.cshtml");
    folderConfig.SanitizeHtml = false;
});

builder.Services.AddMvc()
    .AddApplicationPart(typeof(MarkdownPageProcessorMiddleware).Assembly)
    .AddRazorPagesOptions(options => {options.Conventions.AddPageRoute("/sitemap", "/sitemap.xml");})
    ;

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.WebHost.ConfigureKestrel(serverOptions => {
    serverOptions.Listen(System.Net.IPAddress.Any, 8080);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
    app.UseHttpsRedirection();
}

app.UseMarkdown();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
