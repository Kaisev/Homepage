import { test, expect } from '@playwright/test';

test('Home link takes you home', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).toBeVisible();
  await page.getByRole('link', { name: 'How to protect yourself online Kaisev\'s article on protecting yourself from malware and basic Internet Security. This is a guide meant for beginners' }).click();
  await page.getByRole('link', { name: '1. When (Not) to Trust Someone Learn about trust and how to protect yourself from scammers and phishers Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).not.toBeVisible();
  await page.getByRole('link', { name: 'Kaisev Logo Kaisev' }).click();
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).toBeVisible();
  await page.getByRole('link', { name: 'Resources' }).click();
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).not.toBeVisible();
  await page.getByRole('link', { name: 'Kaisev Logo Kaisev' }).click();
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).toBeVisible();

});
test('Main Page Content displays', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await expect(page.getByRole('heading', { name: 'What is Kaisev?' })).toBeVisible();
  await page.getByRole('heading', { name: 'Kaisev Users' }).click();
  await expect(page.getByText('If you have been given access to Kaisev services, this section is for you!')).toBeVisible();
  await page.getByRole('heading', { name: 'Kaisev Users' }).click();
  await expect(page.getByText('If you live in my house, or have otherwise been given access to Kaisev services,')).not.toBeVisible();
  await page.getByRole('heading', { name: 'Kaisev Users' }).click();
});

test('Resource page and show/hide', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await page.getByRole('link', { name: 'Resources' }).click();
  await expect(page.getByText('This page is a collection of software and links that are useful, or that we recommend')).toBeVisible();
  await page.getByRole('heading', { name: 'Resource for PC Builders' }).click();
  await expect(page.getByRole('heading', { name: 'Build Guides' })).toBeVisible();
  await page.getByRole('heading', { name: 'Resource for PC Builders' }).click();
  await expect(page.getByRole('heading', { name: 'Build Guides' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Windows Applications' }).click();
  await expect(page.getByRole('heading', { name: 'Chocolatey' })).toBeVisible();
  await page.getByRole('heading', { name: 'Windows Applications' }).click();
  await expect(page.getByRole('heading', { name: 'Chocolatey' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Android Applications' }).click();
  await expect(page.getByRole('heading', { name: 'F-Droid' })).toBeVisible();
  await page.getByRole('heading', { name: 'Android Applications' }).click();
  await expect(page.getByRole('heading', { name: 'F-Droid' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Browser Add-ons' }).click();
  await expect(page.getByRole('heading', { name: 'BitWarden' })).toBeVisible();
  await page.getByRole('heading', { name: 'Browser Add-ons' }).click();
  await expect(page.getByRole('heading', { name: 'BitWarden' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Server Tools and Self Hosted Applications' }).click();
  await expect(page.getByRole('heading', { name: 'Web Server Security Tools' })).toBeVisible();
  await page.getByRole('heading', { name: 'Server Tools and Self Hosted Applications' }).click();
  await expect(page.getByRole('heading', { name: 'Web Server Security Tools' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Games' }).click();
  await expect(page.getByRole('heading', { name: 'Super Smash Bros Project M 3.6' })).toBeVisible();
  await page.getByRole('heading', { name: 'Games' }).click();
  await expect(page.getByRole('heading', { name: 'Super Smash Bros Project M 3.6' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Web Design' }).click();
  await expect(page.getByRole('heading', { name: 'Developer References' })).toBeVisible();
  await page.getByRole('heading', { name: 'Web Design' }).click();
  await expect(page.getByRole('heading', { name: 'Developer References' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Scripts' }).click();
  await expect(page.getByRole('heading', { name: 'LazyWinAdmin GUI' })).toBeVisible();
  await page.getByRole('heading', { name: 'Scripts' }).click();
  await expect(page.getByRole('heading', { name: 'LazyWinAdmin GUI' })).not.toBeVisible();
  await page.getByRole('link', { name: 'Home' }).click();
  await expect(page.getByText('This page is a collection of software and links that are useful, or that we recommend')).not.toBeVisible();
});

test('Security article links and read status', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await page.getByRole('link', { name: 'How to protect yourself online Kaisev\'s article on protecting yourself from malware and basic Internet Security. This is a guide meant for beginners' }).click();
  await expect(page.getByRole('heading', { name: 'How to protect yourself online' })).toBeVisible();
  await page.getByRole('link', { name: '1. When (Not) to Trust Someone Learn about trust and how to protect yourself from scammers and phishers Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'Phishing and Trust' })).toBeVisible();
  await page.getByRole('link', { name: 'Mark read and return to article list' }).click();
  await expect(page.getByRole('link', { name: '1. When (Not) to Trust Someone Learn about trust and how to protect yourself from scammers and phishers Written by Jonathan Daigle | 2023-04-24' })).toHaveClass("read");
  await page.getByRole('link', { name: '2. How to Read Web Addresses (URLs) Learn how to read web addresses and how to use this knowledge to protect yourself from attacks Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'Web Addresses' })).toBeVisible();
  await page.getByRole('link', { name: 'Mark read and return to article list' }).click();
  await expect(page.getByRole('link', { name: '2. How to Read Web Addresses (URLs) Learn how to read web addresses and how to use this knowledge to protect yourself from attacks Written by Jonathan Daigle | 2023-04-24' })).toHaveClass("read");
  await page.getByRole('link', { name: '3. Securing Your Accounts Learn how to make your accounts safe from other people and easier for you to access Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'Securing Your Accounts' })).toBeVisible();
  await page.getByRole('link', { name: 'Mark read and return to article list' }).click();
  await expect(page.getByRole('link', { name: '3. Securing Your Accounts Learn how to make your accounts safe from other people and easier for you to access Written by Jonathan Daigle | 2023-04-24' })).toHaveClass("read");
  await page.getByRole('link', { name: '4. File Extensions Learn how to tell the difference between file types and which types could harm your computer Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'Learn Your File Extensions' })).toBeVisible();
  await page.getByRole('link', { name: 'Mark read and return to article list' }).click();
  await expect(page.getByRole('link', { name: '4. File Extensions Learn how to tell the difference between file types and which types could harm your computer Written by Jonathan Daigle | 2023-04-24' })).toHaveClass("read");
  await page.getByRole('link', { name: '5. Recommended Security Software Learn about what software you can use to make your computing experience safer, faster, and easier Written by Jonathan Daigle | 2023-04-24' }).click();
  await expect(page.getByRole('heading', { name: 'Recommended Security Software' })).toBeVisible();
  await page.getByRole('heading', { name: 'For Desktops' }).click();
  await expect(page.getByRole('heading', { name: 'Chocolatey' })).toBeVisible();
  await page.getByRole('heading', { name: 'For Desktops' }).click();
  await page.getByRole('heading', { name: 'For Smartphones/Tablets' }).click();
  await expect(page.getByRole('heading', { name: 'Firefox for Android' })).toBeVisible();
  await page.getByRole('heading', { name: 'For Smartphones/Tablets' }).click();
  await expect(page.getByRole('heading', { name: 'Firefox for Android' })).not.toBeVisible();
  await page.getByRole('heading', { name: 'Browser Add-ons' }).click();
  await expect(page.getByRole('heading', { name: 'BitWarden' })).toBeVisible();
  await page.getByRole('heading', { name: 'Browser Add-ons' }).click();
  await expect(page.getByRole('heading', { name: 'BitWarden' })).not.toBeVisible();
  await page.getByRole('link', { name: 'Return to article list' }).click();
  await expect(page.getByRole('link', { name: '5. Recommended Security Software Learn about what software you can use to make your computing experience safer, faster, and easier Written by Jonathan Daigle | 2023-04-24' })).toHaveClass("read");
});

test('Article links and read status', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await page.getByRole('link', { name: 'Articles' }).click();
  await expect(page.getByRole('heading', {name: 'Playbooks'})).toBeVisible();
  const details = page.locator('details');
  for (let i = 0; i < await details.count(); i++) {
    await details.nth(i).click();
  }
  const articles = page.locator('.blocklist li a');
  for (let i = 0; i < await articles.count(); i++) {
    await expect(articles.nth(i)).not.toHaveClass('read')
    if (!await articles.nth(i).isVisible()) {
      for (let i = 0; i < await details.count(); i++) {
        await details.nth(i).click();
      }
    }
    await articles.nth(i).click();
    await expect(page.getByRole('heading', { level: 1 }).filter({hasNotText: "Kaisev"}).first()).toBeVisible();
    await page.getByRole('link', { name: 'Mark read and return to article list' }).click();
    await expect(articles.nth(i)).toHaveClass('read')
  }
});

test('Privacy policy and license links work', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await page.getByRole('link', { name: 'privacy policy' }).click();
  await expect(page.getByRole('heading', { name: 'Privacy Policy' })).toBeVisible();
  await page.getByRole('link', { name: 'Reciprocal Public License 1.5 (RPL-1.5)' }).click();
  await expect(page).toHaveTitle(/Reciprocal Public License 1.5/);
});

test('Mailto hover works', async ({ page }) => {
  await page.goto('http://localhost:8080/');
  await page.getByRole('link', { name: 'Email icon' }).hover();
  await expect(page.getByText('admin@kaisev.net')).toBeVisible();
});

test('Sitemap is generated', async ({ page }) => {
  await page.goto('http://localhost:8080/sitemap');
  //await expect(page.getByText("/Articles/Security/file_extensions</loc>")).toContain("/Articles/Security/file_extensions</loc>");
  //await expect(page).toContain("/Index</loc>");
  //await expect(page).toContain("/Privacy</loc>");
  //await expect(page).toContain("/Resources</loc>");
  await page.goto('http://localhost:8080/sitemap.xml');
});

test('Articles page works with or without trailing slash', async ({ page }) => {
  await page.goto('http://localhost:8080/Articles/');
  await page.locator(".blocklist li a").first().click();
  await expect(page.getByRole('heading', { level: 1 }).filter({hasNotText: "Kaisev"}).first()).toBeVisible();
  await page.goto('http://localhost:8080/Articles');
  await page.locator(".blocklist li a").first().click();
  await expect(page.getByRole('heading', { level: 1 }).filter({hasNotText: "Kaisev"}).first()).toBeVisible();
});

test('Security page works with or without trailing slash', async ({ page }) => {
  await page.goto('http://localhost:8080/Articles/Security/');
  await page.locator(".blocklist li a").first().click();
  await expect(page.getByRole('heading', { level: 1 }).filter({hasNotText: "Kaisev"})).toBeVisible();
  await page.goto('http://localhost:8080/Articles/Security');
  await page.locator(".blocklist li a").first().click();
  await expect(page.getByRole('heading', { level: 1 }).filter({hasNotText: "Kaisev"})).toBeVisible();
});